import React, { Component } from 'react'
import { connect } from 'react-redux'
import gql from "graphql-tag";
import { Mutation, Query } from "react-apollo";

import { reduxForm } from 'redux-form'
import { replace } from 'react-router-redux'

import { Button } from 'reactstrap'

import SectionNav from '../../components/nav/SectionNav'

import './EditTemplate.css'
import 'codemirror/mode/javascript/javascript'
import 'codemirror/mode/htmlmixed/htmlmixed'
import TemplateContext from './TemplateContext'

import EditTemplateForm from './EditTemplateForm'
import EditTemplateTabs from './EditTemplateTabs'
import Loader from 'react-loader'
import swal from 'sweetalert2'

class EditTemplate extends Component {
  state = {
    saving: false,
    isEditTemplateFormOpen: false,
  }

  render() {

    const {
      state: {
        isEditTemplateFormOpen
      },
      toggle,
      props: {
        match: {
          params: {
            projectId,
            templateId
          }
        }
      }
    } = this
    let template = {}
    return (
      <div>
        <Query
          query={PROJECT_TEMPLATE}
          variables={{id: templateId, projectId}}
          fetchPolicy="network-only"
        >
          {({ loading, error, data }) => {
            if (!loading && data.Project) {
              template = data.Project.Template
            }
            return (
              <Loader loaded={!loading}>
                <SectionNav
                  title={template ? `${template.name} (${template.id})` : "Edit Template"}
                  subtitle={template ? `for ${template.displayMode}` : ""}
                  fluid
                >
                  <Button outline onClick={this.openEditTemplateForm}>
                    <i className="fa fa-edit" aria-hidden="true"></i>
                  </Button>{' '}
                  {
                    (data && data.Project) &&
                    <EditTemplateForm
                      isOpen={isEditTemplateFormOpen}
                      onClose={this.closeEditTemplateForm}
                    />
                  }
                  <Mutation mutation={DELETE_TEMPLATE}>
                    {(deleteTemplate) => (
                      <React.Fragment>
                        <Button color="danger" outline onClick={() => this.deleteTemplate(deleteTemplate, data.Project)}>
                          <i className="fa fa-trash" aria-hidden="true"></i>
                        </Button> {' '}
                      </React.Fragment>
                    )}
                  </Mutation>

                  {this.state.saving
                    ?
                      <Button className="EditTemplate-button">Saving</Button>
                    :
                    <Mutation mutation={UPDATE_TEMPLATE}>
                      {(updateTemplate) => (
                        <Button
                          color="primary"
                          onClick={() =>  this.saveTemplate(updateTemplate, data.Project)}
                          className="EditTemplate-button"
                        >
                        Save Template
                        </Button>
                      )}
                    </Mutation>
                  }
                </SectionNav>
                  {
                    !loading && data.Project &&
                      <TemplateContext.Provider
                        value={{
                          project: data.Project
                        }}
                      >
                        <EditTemplateTabs />
                      </TemplateContext.Provider>
                  }
              </Loader>
            )
          }}
        </Query>
      </div>
    )
  }

  saveTemplate = (mutate, project) => {

    this.setState({ saving: true })

    window.unlayer.saveDesign(async design => {
      try {
        await mutate({
          variables: {
            id: project.Template.id,
            name: project.Template.name,
            design: design,
            displayMode: project.Template.displayMode,
          }
        })
      } catch ({graphQLErrors}) {
        swal({
          title: `Update Template`,
          text: graphQLErrors.map((err) => err.message).join(". "),
          type: 'error',
        })
      }
      this.setState({ saving: false })
    })
  }

  openEditTemplateForm = async () => {
    this.setState({
      isEditTemplateFormOpen: true,
    })
  }

  closeEditTemplateForm = async () => {
    this.setState({
      isEditTemplateFormOpen: false,
    })
  }

  deleteTemplate = (mutate, project) => {
    const { replace } = this.props

    swal({
      title: 'Are you sure?',
      text: "Are you sure you want to delete this template?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Delete Template'
    }).then(async result => {
      if (result.value) {
        const {
          props: {
            match: {
              params: {
                projectId
              }
            }
          }
        } = this
        try {
          const {data} = await mutate({
            variables: {
              id: project.Template.id
            }
          })
          replace(`/projects/${projectId}/templates`)

        } catch ({graphQLErrors}) {
          swal({
            title: `Update Template`,
            text: graphQLErrors.map((err) => err.message).join(". "),
            type: 'error',
          })
        }
      }
    })
  }
}

EditTemplate = reduxForm({
  form: 'editTemplate',
  enableReinitialize: true
})(EditTemplate)

const UPDATE_TEMPLATE = gql`
`

const PROJECT_TEMPLATE = gql`
`

const DELETE_TEMPLATE = gql`
`

const mapDispatchToProps = (dispatch) => {
  return {
    replace(location) { return dispatch(replace(location)) }
  }
}

EditTemplate = connect(
  null,
  mapDispatchToProps
)(EditTemplate)

export default EditTemplate
