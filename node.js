// Built-in Modules
const express = require('express')
const router = express.Router()
const appRoot = require('app-root-path')
const {categoryHelpers} = require(`${appRoot}/server/helpers`)
const asyncMiddleware = require(`${appRoot}/server/middlewares/asyncMiddleware`);

router.post('/', asyncMiddleware(async (req, res, next) => {
  const {
		body: {
			category
		}
	} = req
  const catParams = await categoryHelpers.mapCatParam(category)
  const result = await categoryHelpers.createCategory(catParams)
  res.json(result)
}))

router.get('/', asyncMiddleware(async (req, res, next) => {
  const result = await categoryHelpers.getAllCategories({})
  res.json(result)
}))

router.get('/:id', asyncMiddleware(async (req, res, next) => {
  const {
		params: {
			id
		}
	} = req
	
	const result = await categoryHelpers.getCategoryDetails({where: {id: id}})
  res.json(result)
}))

router.get('/:catType', asyncMiddleware(async (req, res, next) => {
  const {
		params: {
			catType
		}
	} = req
	
	const result = await categoryHelpers.getAllCategories({categoryType: catType})
  res.json(result)
}))

router.put('/:id', asyncMiddleware(async (req, res, next) => {
  const {
		body: {
			category
		},
		params: {
			id: CategoryId
		}
	} = req
	
	const catParams = await categoryHelpers.mapCatParam(category)
  const result = await categoryHelpers.updateCategory(catParams, {where: {id: CategoryId}})
  res.json(result)
}))

router.delete('/:id', asyncMiddleware(async (req, res, next) => {
  const {
		params: {
			id: CategoryId
		}
	} = req
	
	const result = await categoryHelpers.removeCategory({id: CategoryId})
  res.json(result)
}))

module.exports = router
