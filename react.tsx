import React, { useState, useContext } from 'react';
import { RouteComponentProps } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import FolderImage from '../../../../images/folder.png';
import NewFolderDialog from './NewFolderDialog';
import NewTemplateDialog from './NewTemplateDialog';
import TemplateItem from './TemplateItem';
import { AppContext } from '../../../../contexts';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { SectionLayout, SnackbarContentWrapper } from '../../layouts';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import {
  ProjectWithTemplatesQuery,
  ProjectWithTemplatesQueryVariables,
} from './__generated__/ProjectWithTemplatesQuery';
import SectionNav from '../../layouts/SectionNav';
import { CardMedia, Typography } from '@material-ui/core';

const PROJECT_WITH_TEMPLATES_QUERY = gql`
  query ProjectWithTemplatesQuery($id: Int!) {
    Project(id: $id) {
      id
      name
      folders {
        id
        name
        _templatesMeta {
          count
        }
      }
      templates {
        id
        name
        displayMode
        updatedAt
      }
    }
  }
`;

const useStyles = makeStyles((theme) => ({
  folderIcon: {
    height: 50,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'contain',
  },
  card: {
    cursor: 'pointer',
    padding: 15,
    textAlign: 'center',
  },
  folderName: {
    paddingTop: 20,
  },
  typography: {
    marginBottom: '1.35em',
  },
}));

interface RouteParams {}

const MainPage: React.FC<RouteComponentProps<RouteParams>> = ({
  match,
  history,
}) => {
  const appContext = useContext(AppContext);
  const classes = useStyles();

  const [isNewFolderDialogVisible, setNewFolderDialogVisible] = useState(false);
  const [isNewTemplateDialogVisible, setNewTemplateDialogVisible] = useState(
    false
  );
  return (
    appContext.project && (
      <React.Fragment>
        <SectionLayout>
          <SectionNav title="Templates">
            <Button
              variant="contained"
              onClick={() => setNewFolderDialogVisible(true)}
              disabled={isNewFolderDialogVisible}
              color="primary"
            >
              New Folder
            </Button>{' '}
            {isNewFolderDialogVisible && (
              <NewFolderDialog
                onClose={() => setNewFolderDialogVisible(false)}
                onComplete={(folderId) => {
                  setNewFolderDialogVisible(false);
                  history.push(
                    `${match.url.replace('templates', 'folder')}/${folderId}`
                  );
                }}
              />
            )}
            <Button
              variant="contained"
              onClick={() => setNewTemplateDialogVisible(true)}
              disabled={isNewTemplateDialogVisible}
              color="primary"
            >
              New Template
            </Button>{' '}
            {isNewTemplateDialogVisible && (
              <NewTemplateDialog
                onClose={() => setNewTemplateDialogVisible(false)}
                onComplete={(templateId) => {
                  setNewTemplateDialogVisible(false);
                  history.push(`${match.url}/${templateId}`);
                }}
              />
            )}
          </SectionNav>

          <Query<ProjectWithTemplatesQuery, ProjectWithTemplatesQueryVariables>
            query={PROJECT_WITH_TEMPLATES_QUERY}
            variables={{ id: appContext.project.id }}
          >
            {({ data, loading, error }) => {
              if (loading) {
                return <CircularProgress />;
              }

              if (error) {
                return <Grid>Error: {error.message}</Grid>;
              }
              const project = data && data.Project;
              return (
                project && (
                  <React.Fragment>
                    <Grid container spacing={4}>
                      {project.folders.length === 0 && (
                        <Grid item xs={12}>
                          <Typography
                            variant="subtitle1"
                            gutterBottom
                            className={classes.typography}
                          >
                            <SnackbarContentWrapper
                              variant="warning"
                              message="<strong>No Folders!</strong> Create your first folder."
                            />
                          </Typography>
                        </Grid>
                      )}
                      {project.folders.length > 0 && (
                        <Grid item xs={12}>
                          <Typography variant="h6" component="h4">
                            Folders
                          </Typography>
                        </Grid>
                      )}

                      {project.folders.map((folder) => (
                        <Grid item xs={2} key={folder.id}>
                          <Card
                            className={classes.card}
                            onClick={() =>
                              history.push(
                                `${match.url.replace('templates', 'folder')}/${
                                  folder.id
                                }`
                              )
                            }
                          >
                            <CardMedia
                              image={FolderImage}
                              title={folder.name}
                              className={classes.folderIcon}
                            />

                            <Typography
                              variant="h6"
                              component="h5"
                              className={classes.folderName}
                            >
                              {folder.name}
                            </Typography>
                            <Typography
                              variant="overline"
                              display="block"
                              gutterBottom
                            >
                              {folder._templatesMeta &&
                                folder._templatesMeta.count}{' '}
                              Templates
                            </Typography>
                          </Card>
                        </Grid>
                      ))}
                    </Grid>

                    <Grid container spacing={4}>
                      {project.templates.length === 0 && (
                        <Grid item xs={12}>
                          <Typography
                            variant="subtitle1"
                            gutterBottom
                            className={classes.typography}
                          >
                            <SnackbarContentWrapper
                              variant="warning"
                              message="<strong>No Templates!</strong> Create your first template"
                            />
                          </Typography>
                        </Grid>
                      )}
                      {project.templates.length > 0 && (
                        <Grid item xs={12}>
                          <Typography variant="h6" component="h4">
                            Templates
                          </Typography>
                        </Grid>
                      )}

                      {project.templates.map((template) => (
                        <Grid item xs={3} key={template.id}>
                          <TemplateItem
                            template={template}
                            url={match.url}
                            projectId={project.id}
                          />
                        </Grid>
                      ))}
                    </Grid>
                  </React.Fragment>
                )
              );
            }}
          </Query>
        </SectionLayout>
      </React.Fragment>
    )
  );
};

export default MainPage;
